---
title: Local DATA SOURCE Configuration
subtitle: data source setup
caption: DATA Config
author: --
date: 2021-10-29
---
|                                             |                                                     |                           |                            |                                |
|---------------------------------------------|-----------------------------------------------------|---------------------------|----------------------------|--------------------------------|
| [Next: CI/CD Pipelines](../pipes/README.md) | [Prev: DEVELOPMENT Environment](../devel/README.md) | [Up: Usage](../README.md) | [[Contents]](../README.md) | [[Index]](../_index/README.md) |

Data Source Configuration
-------------------------

- [Local DATA](local/README.md): Local file-system Data (Files) configuration
