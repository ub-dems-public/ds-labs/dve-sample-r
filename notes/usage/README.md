---
title: Project Usage Guide
subtitle: development reference manual
caption: Usage Guide
author: --
date: 2021-10-29
---
|                                      |                          |                           |                               |
|--------------------------------------|--------------------------|---------------------------|-------------------------------|
| [Next: QUICK Start](quick/README.md) | [UP:(dir)](../README.md) | [[Contents]](./README.md) | [[Index]](./_index/README.md) |

Project Usage Guide
===================

1. [QUICK Start](quick/README.md)
1. [DEVELOPMENT environment](devel/README.md)
   1. [Introduction](devel/intro/README.md)
      - ["legacy" environment](devel/intro/legacy/README.md)
      - ["containerized" environment](devel/intro/containers/README.md)
   1. [Dependency Management](devel/libs/README.md)
      - ["renv" managed dependency management](devel/libs/deps-renv/README.md)
      - [unmanaged dependency management](devel/libs/deps-no-renv/README.md)
   1. [Project Actions](devel/actions/README.md)
      - ["build" actions](devel/actions/build/README.md)
      - ["runtime" actions](devel/actions/runtime/README.md)
      - ["worker" actions](devel/actions/worker/README.md)
      - ["starter" actions](devel/actions/starter/README.md)
   1. [Execution Context](devel/exec/README.md)
      - ["runner" actions](devel/exec/runner/README.md)
1. [DATA SOURCE configuration](data/README.md)
   1. [Local file-system data](data/local/README.md)
1. [CI/CD Pipelines](pipes/README.md)
   1. [Jenkins Pipelines](pipes/jenkins/README.md)
1. [GIT Tutorials](tutor/README.md)
   1. [GitLab Tutorial](tutor/gitlab/README.md)
   
   

