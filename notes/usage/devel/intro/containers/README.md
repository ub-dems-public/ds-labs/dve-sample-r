---
title: "containerized" development environmnt
subtitle: podman based setup 
caption: devel - podman
author: --
date: 2021-10-29
---
|                                                              |   |                                                |                                  |                                      |
|--------------------------------------------------------------|---|------------------------------------------------|----------------------------------|--------------------------------------|
|  | [Prev: "legacy" environment](../legacy/README.md)  | [Up: Development Overview](../README.md) | [[Contents]](../../../README.md) | [[Index]](../../../_index/README.md) |


Containerized Environment
=========================

This development model is now the "default" model for project usage, described in:

- [Library Dependencies](../../libs/README.md)
- [Project Actions](../../actions/README.md) for "Containerized" commands
- [Execution Context](../../exec/README.md) for project scripts execution
