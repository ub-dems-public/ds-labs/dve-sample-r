---
title: Execution Context
subtitle: script/job execution context
caption: Execution
author: --
date: 2021-10-29
---
|   |                                                   |                                                   |                               |                                   |
|---|---------------------------------------------------|---------------------------------------------------|-------------------------------|-----------------------------------|
|   | [Prev: Development Actions](../actions/README.md) | [Up: DEVELOPMENT Environment](../README.md) | [[Contents]](../../README.md) | [[Index]](../../_index/README.md) |

Execution Context
-----------------

The execution context supports custom `R` script execution.

Default configuration execute a generic "wrapper" script that enable job logging and performance metric collection.
It also supports configuration based script parameter customization and versioning.

  - ["runner" actions](runner/README.md): to initialize project runtime environment and to runs checks and tests


