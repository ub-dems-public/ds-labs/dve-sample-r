---
title: Development Commands (Actions)
subtitle: project build/runtime/deploy/execution actions
caption: Actions
author: --
date: 2021-10-29
---
|                                              |                                                  |                                                   |                               |                                   |
|----------------------------------------------|--------------------------------------------------|---------------------------------------------------|-------------------------------|-----------------------------------|
| [Next: Execution Context](../exec/README.md) | [Prev: Dependency Management](../libs/README.md) | [Up: Development Environment](../README.md) | [[Contents]](../../README.md) | [[Index]](../../_index/README.md) |

Development Commands (Actions)
------------------------------

The development environment defines several actions for the projects, grouped in 4 categories:

- ["build" actions](build/README.md): to initialize project runtime environment and to runs checks and tests
- ["runtime" actions](runtime/README.md): for "containerized" projects only, to control R/RStudio (customized) container execution
- ["worker" actions](worker/README.md): for "containerized" projects only, to embed and run project code in a executable image 
- ["starter" actions](starter/README.md): project execution "entry-point", to trigger scripts invocations or service startup (shiny, plumber)

