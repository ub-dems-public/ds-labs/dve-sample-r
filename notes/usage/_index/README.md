---
title: Project Usage Guides - Index
subtitle: quick start meno
caption: _Index
author: --
date: 2021-10-29
---
|   |                           |                            |                        |
|---|---------------------------|----------------------------|------------------------|
|   | [Up: Usage](../README.md) | [[Contents]](../README.md) | [[Index]](./README.md) |

Project Usage Guides - Index
============================

| Group                                                                   | Entry                                                                                        | Section                                                            |
|-------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| [/](../../../../notes/../)                                              |                                                                                              |                                                                    |
|                                                                         | [DESCRIPTION](../../../../DESCRIPTION)                                                       | [R Packages/Metadata](https://r-pkgs.org/Metadata.html)            |
|                                                                         | [README.Rmd](../../../../README.Rmd)                                                         | ["build" actions](../devel/actions/build/README.md)                |
|                                                                         | [Makefile](../../../../Makefile)                                                             | ["build" actions](../devel/actions/build/README.md)                |
|                                                                         | [build.sh](../../../../build.sh)                                                             | ["build" actions](../devel/actions/build/README.md)                |
|                                                                         | [runtime.sh](../../../../runtime.sh)                                                         | ["runtime" actions](../devel/actions/runtime/README.md)            |
|                                                                         | [worker.sh](../../../../worker.sh)                                                           | ["worker" actions](../devel/actions/worker/README.md)              |
|                                                                         | [starter.sh](../../../../starter.sh)                                                         | ["starter" actions](../devel/actions/starter/README.md)            |
|                                                                         |                                                                                              |                                                                    |
| [/renv](../../../../renv)                                               |                                                                                              |                                                                    |
|                                                                         | [DESCRIPTION](../../../../DESCRIPTION)                                                       | [managed dependency management](../devel/libs/deps-renv/README.md) |
|                                                                         | [renv.lock](../../../../renv.lock)                                                           | [managed dependency management](../devel/libs/deps-renv/README.md) |
|                                                                         | [setting.dcf](../../../../renv/settings.dcf)                                                 | [managed dependency management](../devel/libs/deps-renv/README.md) |
|                                                                         |                                                                                              |                                                                    |
| [/docker/r-images/scripts](../../../../docker/r-images/scripts)         |                                                                                              |                                                                    |
|                                                                         | [install_ubs-runtime.sh](../../../../docker/r-images/scripts/runtime/install_ubs-runtime.sh) | [dependency management](../devel/libs/README.md)                   |
| [/docker/r-images/dockerfiles](../../../../docker/r-images/dockerfiles) |                                                                                              |                                                                    |
|                                                                         | [anchor.Dockerfile](../../../../docker/r-images/dockerfiles/anchor.Dockerfile)               | ["containerized" environment](../devel/intro/containers/README.md) |
|                                                                         | [base.Dockerfile](../../../../docker/r-images/dockerfiles/base.Dockerfile)                   | ["containerized" environment](../devel/intro/containers/README.md) |
|                                                                         |                                                                                              | ["build" actions](../devel/actions/build/README.md)                |
|                                                                         | [runtime.Dockerfile](../../../../docker/r-images/dockerfiles/runtime.Dockerfile)             | ["containerized" environment](../devel/intro/containers/README.md) |
|                                                                         |                                                                                              | ["runtime" actions](../devel/actions/runtime/README.md)            |
|                                                                         | [worker.Dockerfile](../../../../docker/r-images/dockerfiles/worker.Dockerfile)               | ["containerized" environment](../devel/intro/containers/README.md) |
|                                                                         |                                                                                              | ["worker" actions](../devel/actions/worker/README.md)              |
|                                                                         |                                                                                              |                                                                    |
| [/docker/r-images](../../../../docker/r-images)                         |                                                                                              |                                                                    |
|                                                                         | [Makefile](../../../../docker/r-images/Makefile)                                             | ["build" actions](../devel/actions/build/README.md)                |
|                                                                         |                                                                                              | ["runtime" actions](../devel/actions/runtime/README.md)            |
|                                                                         |                                                                                              | ["worker" actions](../devel/actions/worker/README.md)              |
|                                                                         |                                                                                              | ["starter" actions](../devel/actions/starter/README.md)            |
|                                                                         |                                                                                              |                                                                    |
| [/exec](../../../../exec)                                               |                                                                                              |                                                                    |
|                                                                         | [runner.sh](../../../../exec/runner.sh)                                                      | ["runner" actions](../devel/exec/runner/README.md)                 |
|                                                                         | [runner.R](../../../../exec/runner.R)                                                        | ["runner" actions](../devel/exec/runner/README.md)                 |
|                                                                         | [example_data_loader.R](../../../../exec/example_data_loader.R)                              | [local file-system data](../data/local/README.md)                  |
|                                                                         |                                                                                              |                                                                    |
| [/inst/extdata](../../../../inst/extdata)                               |                                                                                              |                                                                    |
|                                                                         | [ext/.gitignore](../../../../inst/extdata/ext/.gitignore)                                    | [local file-system data](../data/local/README.md)                  |
|                                                                         |                                                                                              |                                                                    |

