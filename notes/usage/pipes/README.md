---
title: CI/CD Pipelines
subtitle: Batch builds and deployments
caption: CI/CD Pipelines
author: --
date: 2021-10-29
---
|                                           |                                                      |                           |                            |                                |
|-------------------------------------------|------------------------------------------------------|---------------------------|----------------------------|--------------------------------|
| [Next: Git Tutorials](../tutor/README.md) | [Prev: Data Source Configuration](../data/README.md) | [Up: Usage](../README.md) | [[Contents]](../README.md) | [[Index]](../_index/README.md) |


CI/CD Pipelines
---------------

- [jenkins pipes](jenkins/README.md): references to [jenkins](https://www.jenkins.io/) based pipeline
